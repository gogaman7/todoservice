// 3rd party
var hashmap = require('hashmap')

// our libs
var todo = require('./todo')

/**
 * incompleteTodo entity, object that stores the following information
 *  user : String
 *  incompleteTodos: map: {uuid1: 'todo description1', uuid2: 'todo description2'}
 * @param object either todo object, or object from storage (Item), or dto from lambda
 * @returns
 */
function incompleteTodo(object) {
    if (object instanceof todo) {
        this.user = object.user
        if (object.completed) {
            // current todo is already completed
            this.incompleteTodos = new hashmap()
        } else {
            this.incompleteTodos = new hashmap(object.uuid, object.description)
        }
    } else if (object.user.S) {

        // Item object arrived from storage

        this.user = object.user.S
        this.incompleteTodos = new hashmap()
        Object.keys(object.incompleteTodos.M).forEach(function (uuid) {
            this.incompleteTodos.set(uuid, object.incompleteTodos.M[uuid].S)
        }.bind(this))

    } else {
        // Item object arrived from storage

        // check for garbage coming in, just in case
        if (!object.user || !object.incompleteTodos) {
            throw new Error('unknown object. not supported')
        }

        this.user = object.user
        this.incompleteTodos = new hashmap()
        Object.keys(object.incompleteTodos).forEach(function (uuid) {
            this.incompleteTodos.set(uuid, object.incompleteTodos[uuid])
        }.bind(this))
    }
}

/**
 * Function to retrieve Item information
 */
incompleteTodo.prototype.getItem = function () {
    var item = {
        user: {
            S: this.user
        },
        incompleteTodos: {
            M: {}
        }
    }

    this.incompleteTodos.keys().forEach(function (uuid) {
        item.incompleteTodos.M[uuid] = {
            S: this.incompleteTodos.get(uuid)
        }
    }.bind(this))
    return item;
}

/**
 * Function to add an extra todo to existing incompleteTodo (this) object
 * @param todo
 */
incompleteTodo.prototype.addTodo = function (todo) {
    if (!todo.completed) {
        this.incompleteTodos.set(todo.uuid, todo.description)
    } else {
        this.incompleteTodos.remove(todo.uuid)
    }
}

/**
 * Function to retrieve array of all todo descriptions
 */
incompleteTodo.prototype.getAllDescriptions = function () {
    var descriptions = []

    this.incompleteTodos.keys().forEach(function (uuid) {
        descriptions.push(this.incompleteTodos.get(uuid))
    }.bind(this))

    return descriptions
}

module.exports = incompleteTodo