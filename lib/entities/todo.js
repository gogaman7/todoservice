/**
 * Entity that represent todo for a user
 *  the following properties are covered
 *   * uuid, unique id
 *   * user, email address of user
 *   * description, description of todo
 *   * priority (Number), rating of how important this todo is
 * @param object
 */
function todo(object) {
    // verify when object is an Item from storage

    if (object.user.S) {
        // have Item object on our hahnds

        this.uuid = object.uuid.S
        this.user = object.user.S
        this.description = object.description.S
        this.priority = Number(object.priority.N)

        if (object.completed && object.completed.S)
            this.completed = object.completed.S

    } else {
        // have DTO object from lambda function
        this.uuid = object.uuid
        this.user = object.user
        this.description = object.description
        this.priority = object.priority

        if (object.completed)
            this.completed = object.completed
    }
}

module.exports = todo