// 3rd party
var aws = require('aws-sdk')
var async = require('async')
var config = require('config')

// The AWS SDK for Node.js doesn't select the region by default
// http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/node-configuring.html#Setting_the_Region
var awsRegion = config.get('aws.region')
var dynamoDbApiVersion = config.get('aws.dynomodb.apiVersion')

aws.config.update({region: awsRegion})

// locking an api of DynamoDB service to a tested version
var dynamoDB = new aws.DynamoDB({apiVersion: dynamoDbApiVersion})

var docClient = new aws.DynamoDB.DocumentClient({}, dynamoDB);

/**
 * Function to abstract interactions with dynamoDB
 */
function dynamo(options) {
    this.tableName = options.tableName
}

/**
 * Function to list existing tables in dynamoDB
 * @param callback fn that will be called with the following signature: err, []tableNameString
 * @private
 */
dynamo.prototype._listTables = function (params, callback) {
    dynamoDB.listTables(params, callback)
}

dynamo.prototype.init = function (callback) {
    var self = this

    // ExclusiveStartTableName DOES not work :(
    /*var params = {
     ExclusiveStartTableName: this.tableName,
     Limit: 10
     };*/

    async.waterfall([
        async.apply(self._listTables.bind(self)),

        /**
         * Find our table from all table names
         * @param arrayOfTableNames
         * @param callback
         */
        function filterForOurTargetTableName(arrayOfTableNames, callback) {
        async.filterLimit(arrayOfTableNames.TableNames, 100, self._tableNameIterator.bind(self), callback)
        },
        function findOurTable(arrayOfTableNames, callback) {
            if (arrayOfTableNames.length == 1) {
                // table already exists
                callback()
            } else {
                // table does not exist, so lets create it
                self._createTable(callback)
            }
        }
    ], callback)
}

/**
 * Iterator function to find out table
 * @param item
 * @param callback
 * @private
 */
dynamo.prototype._tableNameIterator = function (item, callback) {
    callback(null, (item == this.tableName))
}

/**
 * Function to create table
 * @param callback
 * @private
 */
dynamo.prototype._createTable = function (callback) {
    // Todo: WIP, most params below need to be passed in
    // Todo: it would be nice, if app upon calling of init(), when table is not there,
    //  the table would be created
    callback({message: 'not implemented'})
}

/**
 * Function to put params object, including item into a table
 * @param params object that looks like this:
 * {
    TableName:table,
    Item:{
        "year": year,
        "title": title,
        "info":{
            "plot": "Nothing happens at all.",
            "rating": 0
        }
    }
 * }
 * @param  callback fn that will be called with the following signature: err
 */
dynamo.prototype.put = function (params, callback) {
    docClient.put(params, function (err) {
        callback(err)
    })
}

/**
 * Function to put params object, including item into a table
 * @param params object that looks like this:
 * {
    TableName:table,
    Item:{
        "year": year,
        "title": title,
        "info":{
            "plot": "Nothing happens at all.",
            "rating": 0
        }
    }
 * }
 * @param  callback fn that will be called with the following signature: err
 */
dynamo.prototype.putItem = function (params, callback) {
    dynamoDB.putItem(params, function (err) {
        callback(err)
    })
}

/**
 * Function returns an item with the given primary key
 * @param params object that looks like this:
 * {
 *   TableName: "tableNameHere",
 *   Key: {
 *     someKey: {
 *       S: "someKeyValue"
 *     }
 *   }
 * }
 * @param callback
 */
dynamo.prototype.getItem = function (params, callback) {
    dynamoDB.getItem(params, function (err, response) {
        callback(err, response && response.Item)
    })
}

/**
 * Function returns one or more items and item attributes by accessing every item in a table
 * @param params params for a scan
 *  * see this reference for more details: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/DocumentClient.html#scan-property
 * @param callback fn that will be called with the following signature: err, []Items
 */
dynamo.prototype.scan = function (params, callback) {
    // Todo: handle LastEvaluatedKey and keep calling scan until LastEvaluatedKey is undefined
    docClient.scan(params, function (err, response) {
        if (err) {
            callback(err)
        } else {
            callback(err, response.Items)
        }
    })
}

/**
 * Function to delete one or more items
 * @param params params for a deleteItem operation
 *  * see this reference for more details: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html#deleteItem-property
 * @param callback
 */
dynamo.prototype.deleteItem = function (params, callback) {
    dynamoDB.deleteItem(params, function (err) {
        callback(err)
    })
}
module.exports = dynamo
