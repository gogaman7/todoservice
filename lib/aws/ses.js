// 3rd party
var aws = require('aws-sdk')
var async = require('async')
var config = require('config')

var awsRegion = config.get('aws.region')
var sesApiVersion = config.get('aws.ses.apiVersion')

// The AWS SDK for Node.js doesn't select the region by default
// http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/node-configuring.html#Setting_the_Region
aws.config.update({region: awsRegion})

// locking an api of ses service to a tested version
var ses = new aws.SES({apiVersion: sesApiVersion});

/**
 * Function to abstract ses service interactions
 */
function sesService() {
}

/**
 * Function to send an email out
 * @param options object with the following structure:
 * {
 *   sender: 'sender@domain.com',
 *   recepient: 'receiver@somedomain.com',
 *   body: 'some text for body',
 *   subject: 'some subject line'
 * }
 * @param callback fn that will be called with the following signature: err
 */
sesService.prototype.sendEmail = function (options, callback) {
    var params = {
        Source: options.sender,
        Destination: {ToAddresses: [options.recepient]},
        Message: {
            Subject: {
                Data: options.subject
            },
            Body: {
                Text: {
                    Data: options.body,
                }
            }
        }
    }

    async.waterfall([
        function send(callback) {
            ses.sendEmail(params, function (err) {
                callback(err)
            })
        },
        // since we are in ses sandbox, let's wait at least 2 seconds (requirement is 1)
        function sleep(callback) {
            setTimeout(function () {
                callback()
            }, 2000)
        }
    ], callback)
}

module.exports = new sesService()