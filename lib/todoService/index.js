// 3rd party
var async = require('async')
var moment = require('moment')

// our libs
var storage = require('./storage')
var incompleteTodoService = require('../incompleteTodoService')

var storageInstance = new storage();

/**
 * This function is to provide service API surrounding todo object(s)
 * // Todo: consider creating entities/todo.js with .getItem()
 */
function todoService() {}

/**
 * Function to initialize todoService
 *  - things like, table creation should happen, if not present (NOT IMPLEMENTED)
 * @param callback
 */
todoService.prototype.init = function(callback) {
    storageInstance.init(callback)
}

/**
 * Function to persist a new todo
 *  * followed by creation of incompleteTodo object, if todo is NOT complete
 * @param todo object
 * @param callback fn that will be called with the following signature: err
 */
todoService.prototype.add = function(todo, callback) {
    async.waterfall([
        async.apply(storageInstance.add.bind(storageInstance), todo),
        async.apply(storageInstance.findOneByKey.bind(storageInstance), todo),
        async.apply(incompleteTodoService.createUpdateByTodo.bind(incompleteTodoService))
    ], callback)
}

/**
 * Function to list all todos
 * @param callback fn that will be called with the following signature: err, arrayOfOfTodoObjects
 */
todoService.prototype.list = function(callback) {
    storageInstance.list(callback)
}

/**
 * Function to delete todo object(s)
 * @param options Key object in the following format: { uuid: '8fea7cb2-6f6d-412e-aaa9-010244e564ed'}
 * @param callback fn that will be called with the following signature: err
 */
todoService.prototype.delete = function(options, callback) {
    var self = this

    async.waterfall([
        async.apply(storageInstance.findOneByKey.bind(storageInstance), options),
        function processIncompleteTodos(todoObject, callback) {
            // telling that todo object being deleted, is actually being completed first, 
            //  this way incompleteTodoService can handle it the same way if todoObject is being just updated/created
            todoObject.completed =  moment().format()
            incompleteTodoService.createUpdateByTodo(todoObject, callback) 
        },
        async.apply(storageInstance.delete.bind(storageInstance), options)
    ], callback)
}

module.exports = new todoService()
