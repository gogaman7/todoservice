// node core
var util = require('util')

// 3rd party
var config = require('config')

// our libs
var dynamoDb = require('../../aws/dynamoDb')
var todoModel = require('../../entities/todo')

// Name of the table in dynamoDB
const TABLE_NAME = config.get('aws.dynomodb.todoTableName')

/**
 * This function is to provide storage service API, specifically for todoService
 * // Todo: refactor the common parts with incompleteTodoService/storage service
 */
function storage() {
    this.tableName = TABLE_NAME
}

// Todo: consider composing dynamoDb vs inheritance
util.inherits(storage, dynamoDb);

/**
 * Function to add a new object
 * @param todo todo object
 * @param callback fn that will be called with the following signature: err
 */
storage.prototype.add = function (todo, callback) {
    this.put({
        TableName: TABLE_NAME,
        Item: todo
    }, function (err) {
        callback(err)
    })
}

/**
 * Function to list all objects
 * @param callback fn that will be called with the following signature: err, []Objects
 */
storage.prototype.list = function (callback) {
    this.scan({
        TableName: TABLE_NAME,
        Limit: 10
    }, callback)
}

/**
 * Function to delete an object
 * @param options Key object in the following format: { propertyName: 'propertyValue'}
 * @param callback
 */
storage.prototype.delete = function (options, callback) {
    this.deleteItem({
        TableName: TABLE_NAME,
        Key: this._getAttributeValue(options)
    }, callback)
}

/**
 * Function to find one todo object by key
 * @param key object with the following structure:
 * { uuid: "some-uuid-here-here" }
 * @param callback fn that will be called with the following signature: err, todo (undefined, if not found)
 */
storage.prototype.findOneByKey = function (key, callback) {
    var params = {
        TableName: TABLE_NAME,
        Key: {
            uuid: {
                S: key.uuid
            }
        }
    }

    this.getItem(params, function (err, Item) {
        if (!Item) {
            // undefined
            callback(err, Item)
        } else {
            callback(err, new todoModel(Item))
        }
    })
}

/**
 * Function to generate Attribute value object out of {propertyName: 'propertyValue'} object
 *  Attribute value object should have the following structure:
 *  {
 *    propertyName: {
 *      S: 'propertyValue'
 *    }
 *  }
 * @param options
 * @returns {{}}
 * @private
 */
storage.prototype._getAttributeValue = function (options) {
    var attributeValueObject = {}
    Object.keys(options).forEach(function (property) {
        attributeValueObject[property] = {
            S: options[property]
        }
    })

    return attributeValueObject;
}

module.exports = storage
