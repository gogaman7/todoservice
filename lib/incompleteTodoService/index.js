// 3rd party
var async = require('async')

// our libs
var storage = require('./storage')
var emailService = require('../emailService')

var storageInstance = new storage();
var incompleteTodo = require('../entities/incompleteTodo')
var todo = require('../entities/todo')

/**
 * This function is to provide service API surrounding incompleteTodo object(s)
 */
function incompleteTodoService() {
}

/**
 * Function to initialize incompleteTodoService
 *  - things like, table creation should happen, if not present (NOT IMPLEMENTED)
 * @param callback
 */
incompleteTodoService.prototype.init = function (callback) {
    storageInstance.init(callback)
}

/**
 * Function to persist/update incompleteTodo by examining existing todo
 * @param todo todo object
 * @param callback fn that will be called with the following signature: err
 */
incompleteTodoService.prototype.createUpdateByTodo = function (todoObject, callback) {

    var self = this

    async.waterfall([
        function findCurrentIncompleteTodoObject(callback) {
            self._findOneByTodo(todoObject, function (err, incompleteTodoObject) {
                if (!err && !incompleteTodoObject) {
                    // not found, creating new incompleteTodo out of todoObject
                    callback(null, new incompleteTodo(todoObject))
                } else if (err) {
                    callback(err)
                } else {
                    // update existing incompleteTodo with information from new todoObject
                    incompleteTodoObject.addTodo(todoObject)
                    callback(err, incompleteTodoObject)
                }
            })
        },
        function addToStorage(incompleteTodo, callback) {

            if (incompleteTodo.incompleteTodos.keys().length == 0) {
                // last key in incompleteTodos is removed, need to delete entire incompleteTodo object
                storageInstance.delete({user: incompleteTodo.user}, callback)
            } else {
                // saving new/updated incompleteTodo object back into storage
                storageInstance.add(incompleteTodo, callback)
            }
        }
    ], callback)

}

/**
 * Function to find one incompleteTodo object by todo object
 * @param todo todo object that will be used to look up incompleteTodo object by
 * @param callback fn that will be called with the following signature: err, incompleteTodoObject (undefined, if not found)
 * @private
 */
incompleteTodoService.prototype._findOneByTodo = function (todo, callback) {
    storageInstance.findOneByKey(todo, callback)
}

/**
 * Function to list all incompleteTodo objects
 * @param callback fn that will be called with the following signature: err, []incompleteTodo
 */
incompleteTodoService.prototype.list = function (callback) {
    storageInstance.list(callback)
}

/**
 * Function to notify all users of their incomplete todos
 * @param callback fn that will be called with the following signature: err
 */
incompleteTodoService.prototype.notifyAllUsers = function (callback) {
    // going to retrieve all incompleteTodoObjects and generate an email to each user
    //  DISCLAMER: I realize, this is very inefficient, since entire table will need to fit in memory,
    //   - instead should stream the table out, and on each chunk (Item in table), send an email just for that chunk.
    //  - for sake of simplicity, not implementing above

    var self = this

    async.waterfall([
        async.apply(self.list.bind(self)),
        function sendEmailToEachIncompleteTodo(arrayOfIncompleteTodoObjects, callback) {
            async.eachSeries(arrayOfIncompleteTodoObjects, emailService.sendEmail.bind(emailService), callback)
        }
    ], callback)

}

/**
 * Function to delete incompleteTodo object(s)
 * @param options Key object in the following format: { user: 'ibresume@gmail.com'}
 * @param callback fn that will be called with the following signature: err
 */
incompleteTodoService.prototype.delete = function (options, callback) {
    storageInstance.delete(options, callback)
}

module.exports = new incompleteTodoService()
