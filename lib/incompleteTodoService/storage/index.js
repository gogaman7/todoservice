// node core
var util = require('util')

// 3rd party
var config = require('config')
var async = require('async')

// our libs
var dynamoDb = require('../../aws/dynamoDb')
var incompleteTodo = require('../../entities/incompleteTodo')

// Name of the table in dynamoDB
const TABLE_NAME = config.get('aws.dynomodb.incompleteTodoTableName')

/**
 * This function is to provide storage service API, specifically for incompleteTodoService
 * // Todo: refactor the common parts with todoService/storage service
 */
function storage() {
    this.tableName = TABLE_NAME
}

// Todo: consider composing dynamoDb vs inheritance
util.inherits(storage, dynamoDb);

/**
 * Function to add a new object
 * @param incompleteTodo incompleteTodo object
 * @param callback fn that will be called with the following signature: err
 */
storage.prototype.add = function (incompleteTodo, callback) {
    this.putItem({
        TableName: TABLE_NAME,
        Item: incompleteTodo.getItem()
    }, callback)
}

/**
 * Function to find one inncompleteTodo object by key
 * @param key object with the following structure:
 * { user: "ibresume@gmail.com" }
 * @param callback fn that will be called with the following signature: err, incompleteTodo (undefined, if not found)
 */
storage.prototype.findOneByKey = function (key, callback) {
    this.getItem({
        TableName: TABLE_NAME,
        Key: {
            user: {
                S: key.user
            }
        }
    }, function (err, Item) {
        if (!Item) {
            // undefined
            callback(err, Item)
        } else {
            callback(err, new incompleteTodo(Item))
        }
    })
}

/**
 * Function to list all objects
 * @param callback fn that will be called with the following signature: err, []incompleteTodo
 */
storage.prototype.list = function (callback) {
    var self = this

    this.scan({
        TableName: TABLE_NAME,
        Limit: 10
    }, function (err, arrayOfItems) {
        if (err) {
            callback(err)
        } else if (arrayOfItems.length == 0) {
            callback(err, [])
        } else {
            // convert arrayOfItems
            async.mapSeries(arrayOfItems, self._convertItemIterator.bind(self), callback)
        }
    })
}

/**
 * Iterator to convert Item to incompleteTodo
 * @param Item object to convert
 * @param callback fn that will be called with the following signature: err, incompleteTodo
 * @private
 */
storage.prototype._convertItemIterator = function (Item, callback) {
    callback(null, new incompleteTodo(Item))
}

/**
 * Function to delete an object
 * @param options Key object in the following format: { propertyName: 'propertyValue'}
 * @param callback fn that will be called with the following signature: err
 */
storage.prototype.delete = function (options, callback) {
    this.deleteItem({
        TableName: TABLE_NAME,
        Key: this._getAttributeValue(options)
    }, callback)
}

/**
 * Function to generate Attribute value object out of {propertyName: 'propertyValue'} object
 *  Attribute value object should have the following structure:
 *  {
 *    propertyName: {
 *      S: 'propertyValue'
 *    }
 *  }
 * @param options
 * @returns {{}}
 * @private
 */
storage.prototype._getAttributeValue = function (options) {
    var attributeValueObject = {}
    Object.keys(options).forEach(function (property) {
        attributeValueObject[property] = {
            S: options[property]
        }
    })

    return attributeValueObject;
}

module.exports = storage
