// 3rd party
var config = require('config')

// our libs
var ses = require('../aws/ses')

/**
 * Function to abstract email communication
 * @constructor
 */
function emailService() {
}

/**
 * Function to send an email to incompleteTodo.user (email address)
 * @param incompleteTodo object that has recepient and information in incomplete todo descriptions
 * @param callback fn that will be called with the following signature: err
 */
emailService.prototype.sendEmail = function (incompleteTodo, callback) {
    var arrayOfDescriptions = incompleteTodo.getAllDescriptions()
    var recepient = incompleteTodo.user
    var sender = (config.has('ses.senderEmailAddress') ? config.get('ses.senderEmailAddress') : 'ibresume@gmail.com')

    // Todo: read subject from config
    var options = {
        sender: sender,
        recepient: recepient,
        subject: 'Here are your daily incomplete todos'
    }

    // Todo, read body template ( stored in S3 ), and given arrayOfDescriptions,
    //  build text for body of email
    var body = 'Here are your incomplete todos (descriptions)\n'
    arrayOfDescriptions.forEach(function (todoDescription) {
        body += '* ' + todoDescription + '\n'
    })
    options.body = body

    ses.sendEmail(options, callback)

}

module.exports = new emailService()
