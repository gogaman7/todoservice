// node core
var assert = require('assert')

// 3rd party
var moment = require('moment')
var async = require('async')

// our libs
var storage = require('../../lib/todoService/storage')

describe('suite of tests surrounding todoService storage', function () {
    it('should init okay', function (done) {
        var storageInstance = new storage()
        storageInstance.init(done)
    })

    it('should add, list, update and delete successfully', function (done) {
        var storageInstance = new storage()

        var sampleToDo = {
            uuid: '8fea7cb2-6f6d-412e-aaa9-010244e564ed',
            user: "testy.mctester@example.com",
            description: "Do something awesome",
            priority: 0
        }

        var sampleToDo2 = {
            uuid: '9fea7cb2-6f6d-412e-aaa9-010244e564ed',
            user: "ibresume@gmail.com",
            description: "nail this assignment",
            priority: 1
        }

        // ISO8601 string
        // 2016-07-06T12:22:46-04:00
        var completedTimestamp = moment().format()

        function deleteIterator(todo, callback) {
            storageInstance.delete({uuid: todo.uuid}, callback)
        }

        function deleteAllExistingToDos(arrayOfSavedToDos, callback) {
            async.eachLimit(arrayOfSavedToDos, 10, deleteIterator, callback)
        }

        /* Integration test plan
         * list all current todos
         * delete each todo
         * add sampleToDo
         * add sampleToDo2
         * list all todos
         * make sure the current array matches the 2 todo objects from above
         * delete all existing todos
         */

        async.waterfall([
            async.apply(storageInstance.list.bind(storageInstance)),
            async.apply(deleteAllExistingToDos),
            async.apply(storageInstance.add.bind(storageInstance), sampleToDo),
            async.apply(storageInstance.add.bind(storageInstance), sampleToDo2),
            async.apply(storageInstance.list.bind(storageInstance)),
            function verifyAllTodos(arrayOfTodos, callback) {
                assert.deepEqual(arrayOfTodos, [sampleToDo, sampleToDo2], 'making sure our two samples are persisted just like we expect them to')
                callback(null, arrayOfTodos)
            },
            async.apply(deleteAllExistingToDos)
        ], done)
    })
})
