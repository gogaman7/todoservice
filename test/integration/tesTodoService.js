// node core
var assert = require('assert')

// 3rd party
var moment = require('moment')
var async = require('async')

// our libs
var todoService = require('../../lib/todoService')
var incompleteTodoService = require('../../lib/incompleteTodoService')

describe('suite of tests surrounding todoService', function () {
    it('should handle a complex flow', function (done) {

        this.timeout(20000)

        function deleteIterator(todo, callback) {
            todoService.delete({uuid: todo.uuid}, callback)
        }

        function deleteAllExistingToDos(arrayOfSavedToDos, callback) {
            async.eachLimit(arrayOfSavedToDos, 10, deleteIterator, callback)
        }


        function deleteIncompleteTodoIterator(incompleteTodo, callback) {
            incompleteTodoService.delete({user: incompleteTodo.user}, callback)
        }

        function deleteAllExistingIncompleteToDos(arrayOfSavedIncompleteToDos, callback) {
            async.eachLimit(arrayOfSavedIncompleteToDos, 10, deleteIncompleteTodoIterator, callback)
        }


        /**
         * Integration test plan
         * - list all todos
         *   * delete each todoObject
         * - list all incompleteTodos
         *   * delete each incompleteTodo

         * - add completed todoObject1 (for user1)
         * - list all todos
         * - make sure same todoObject1 has been returned
         * - list all incompleteTodos
         * - make sure there are NONE
         * - add incomplete todoObject2 (for user1)
         * - list all todos
         * - make sure [todoObject1, todoObject2] has been returned
         * - list all incompleteTodos
         * - make sure there one, that contains todoObject2.description only
         * - add/update todoObject2 (while .complete = now().format())
         * - list all incompleteTodos
         * - make sure there are NONE
         * list all todos
         * - make sure there are 2 todoObject1 and todoObject2
         * - add/update todoObject (while .complete = undefined)
         * - add/update todoObject2 (while .complete = undefined)
         * - add/update todoObject3
         * - add/update todoObject4
         *        * - call incompleteTodoService.notifyAllUsers()
         */

        var sampleToDo = {
            uuid: '8fea7cb2-6f6d-412e-aaa9-010244e564ed',
            user: "ibresume@gmail.com",
            description: "impress, impress",
            priority: 2,
            completed: moment().format()
        }

        var sampleToDo2 = {
            uuid: '9fea7cb2-6f6d-412e-aaa9-010244e564ed',
            user: "ibresume@gmail.com",
            description: "nail this assignment",
            priority: 1
        }

        var sampleToDo3 = {
            uuid: '1fea7cb2-6f6d-412e-aaa9-010244e564ed',
            user: "gogaspam1@gmail.com",
            description: "learn Go",
            priority: 1
        }

        var sampleToDo4 = {
            uuid: '2fea7cb2-6f6d-412e-aaa9-010244e564ed',
            user: "gogaspam1@gmail.com",
            description: "become Go god",
            priority: 2
        }

        async.waterfall([
            async.apply(todoService.list.bind(todoService)),
            async.apply(deleteAllExistingToDos),

            async.apply(incompleteTodoService.list.bind(todoService)),
            async.apply(deleteAllExistingIncompleteToDos),

            async.apply(todoService.add.bind(todoService), sampleToDo),
            async.apply(todoService.list.bind(todoService)),
            function (arrayOfTodos, callback) {
                assert.deepEqual(arrayOfTodos, [sampleToDo], 'making sure sampleToDo is the only object thats persisted')
                callback()
            },
            async.apply(incompleteTodoService.list.bind(incompleteTodoService)),
            function (arrayOfIncompleteTodos, callback) {
                assert.equal(arrayOfIncompleteTodos.length, 0, 'arrayOfIncompleteTodos is empty')
                callback()
            },
            async.apply(todoService.add.bind(todoService), sampleToDo2),
            async.apply(todoService.list.bind(todoService)),
            function (arrayOfTodos, callback) {
                assert.deepEqual(arrayOfTodos, [sampleToDo, sampleToDo2], 'making sure both sampleToDos are returned')
                callback()
            },
            async.apply(incompleteTodoService.list.bind(incompleteTodoService)),
            function (arrayOfIncompleteTodos, callback) {
                assert.equal(arrayOfIncompleteTodos.length, 1, 'arrayOfIncompleteTodos has 1 object')
                assert.equal(arrayOfIncompleteTodos[0].incompleteTodos.values().length, 1, 'making sure we only have one incompleteTodo description')
                assert.equal(arrayOfIncompleteTodos[0].incompleteTodos.values()[0], sampleToDo2.description, 'making sure we have incompleteTodo description matching sampleTodo2.description')

                sampleToDo2.completed = moment().format()
                callback()
            },
            async.apply(todoService.add.bind(todoService), sampleToDo2),
            async.apply(incompleteTodoService.list.bind(incompleteTodoService)),
            function (arrayOfIncompleteTodos, callback) {
                assert.equal(arrayOfIncompleteTodos.length, 0, 'should not have any arrayOfIncompleteTodos, since one todo, just got completed')
                callback()
            },
            async.apply(todoService.list.bind(todoService)),
            function (arrayOfTodos, callback) {
                assert.deepEqual(arrayOfTodos, [sampleToDo, sampleToDo2], 'making sure both sampleToDos are returned')
                delete sampleToDo.completed
                delete sampleToDo2.completed
                callback()
            },
            async.apply(todoService.add.bind(todoService), sampleToDo),
            async.apply(todoService.add.bind(todoService), sampleToDo2),
            async.apply(todoService.add.bind(todoService), sampleToDo3),
            async.apply(todoService.add.bind(todoService), sampleToDo4),
            async.apply(incompleteTodoService.notifyAllUsers.bind(incompleteTodoService))
        ], done)
    })
})