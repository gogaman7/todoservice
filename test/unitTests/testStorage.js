// node core
var assert = require('assert')

// our libs
var storage = require('../../lib/todoService/storage')

describe('suite of tests surrounding storage lib', function () {
    it('should run _getAttributeValue correctly', function () {

        var storageInstance = new storage()

        var uuid = '8fea7cb2-6f6d-412e-aaa9-010244e564ed'
        var expectedAttributeValueObject = {
            uuid: {
                S: uuid
            }
        }

        assert.deepEqual(storageInstance._getAttributeValue({uuid: uuid}), expectedAttributeValueObject, 'making sure we receive object as expected')
    })
})